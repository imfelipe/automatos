package automatos;

import java.awt.Desktop;
import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Principal {
	
    public static void main(String[] args) {				
	Principal t = new Principal();		
        //t.faca1();
        //t.faca2();
        t.visualizar();
    }
    
    public void visualizar(){
        try{
            AFD a = new AFD();
            a.ler("./src/automatos/AFD.XML");
            String jsObject =  a.toJavascript();
            String[] lineArray = Files.readAllLines(Paths.get("./src/automatos/visual.template.html")).toArray(new String[0]);
            String text = String.join("\n",lineArray);
            text = text.replace("{{data}}",jsObject);
            //Files.write nao aceita uma string, só uma lista. Coisas de Java.
            ArrayList<String> listText = new ArrayList<String>();
            listText.add(text);
            File file = new File("src/automatos/visual.html");
            Files.write(Paths.get(file.getAbsolutePath()),listText, Charset.forName("UTF-8"));
            //Abre arquivo html com o programa padrão do OS. (Por default o browser)
            Desktop.getDesktop().open(file);
        }catch(Exception e){
            System.out.println("Exception");
            System.out.println(e.getClass().toString());
        }
    }

    /**
     *  Esse metodo le o arquivo AFD.XML e imprime
     *  seu conteudo formatado.
     */    
    @SuppressWarnings("empty-statement")
    public void faca1() {
        AFD a = new AFD();
        String w = "abaaaa";
        try {
               a.ler("./src/automatos/AFD.XML");
               System.out.println(a);
               // a.gera(a.getEstadoInicial(), "");
               if (a.Aceita(w))
                   System.out.println("Aceitou "+w);
               System.out.println("Pe(q0,"+w+"):"+a.pe(a.getEstadoInicial(),w));
        } catch (Exception e){
               System.out.println(e); 
        } 
    }

    public void faca2() {
        AFNe a = new AFNe();
        try {
               a.ler("./src/automatos/AFNe.xml");
               System.out.println(a);
               System.out.println(a.fecho_e(a.getEstadoInicial()));
               System.out.println(a.fecho_e_e(a.getEstados()));
               ConjuntoEstados ce = new ConjuntoEstados();
               ce.inclui(a.getEstadoInicial());
               System.out.println(a.pe(ce, "ba"));
        } catch (Exception e){
               System.out.println(e);
        }     
    }
}
